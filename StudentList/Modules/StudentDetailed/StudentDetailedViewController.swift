//
//  StudentDetailedViewController.swift
//  StudentList
//
//  Created by Артур on 04.05.2021.
//

import UIKit

class StudentDetailedViewController: UIViewController {

    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var surnameTextField: UITextField!
    @IBOutlet var gradeTextField: UITextField!
    var student: Student?
    
    init(student: Student?) {
        self.student = student
        super.init(nibName: "StudentDetailedViewController", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = student?.surname ?? "Добавить ученика"
        
        let saveButton = UIBarButtonItem(title: "Сохранить", style: .plain, target: self, action: #selector(saveButtonDidTap))
        navigationItem.rightBarButtonItem = saveButton
        
        let cancelButton = UIBarButtonItem(title: "Отмена", style: .plain, target: self, action: #selector(cancelButtonDidTap))
        navigationItem.leftBarButtonItem = cancelButton
        
        nameTextField.placeholder = "Имя"
        surnameTextField.placeholder = "Фамилия"
        gradeTextField.placeholder = "Средняя оценка"
        
        nameTextField.text = student?.name
        surnameTextField.text = student?.surname
        gradeTextField.text = student == nil ? nil : String(student?.grade ?? 0)
    }
    @objc func cancelButtonDidTap() {
        navigationController?.popViewController(animated: true)
    }
    @objc func saveButtonDidTap() {
        guard let name = vaidateText(text: nameTextField.text) else {
            showError(errorString: "Заполните поле имя корректно")
            return
        }
        guard let surname = vaidateText(text: surnameTextField.text) else {
            showError(errorString: "Заполните поле фамилия корректно")
            return
        }
        guard let grade = validateGrade(gradeText: gradeTextField.text) else {
            showError(errorString: "Поле оценки должно содержать целое число от 1 до 5")
            return
        }
        
        if var student = student {
            student.name = name
            student.surname = surname
            student.grade = grade
            RealmManager().write(object: [RealmStudent(student: student)]) // запись текущего студента в базу
        } else {
            let student = Student(name: name, surname: surname, grade: grade)
            RealmManager().write(object: [RealmStudent(student: student)]) // создание нового студента с помощью данных из текстфилдов и запись его в базу
        }
        
        navigationController?.popViewController(animated: true)
    }
    
    func showError(errorString: String) {
        let alert = UIAlertController(title: "Ошибка!", message: errorString, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ок", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    func vaidateText(text: String?) -> String? {
        if let text = text, !text.isEmpty, !text.contains(" ") {
            
            let russianAlphabet = "АБВГДЕЁЖЗИКЛМНОПРСТУФХЦЧШЩЭЮЯЬЪЫЙ"
            let englishAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            
            for char in text {
                if !russianAlphabet.contains(char.uppercased()), !englishAlphabet.contains(char.uppercased()) {
                    return nil
                }
            }
            
            return text
        } else {
            return nil
        }
    }
    
    func validateGrade(gradeText: String?) -> Int? {
        if let gradeText = gradeText, let grade = Int(gradeText), grade > 0, grade < 6 {
            return grade
        } else {
            return nil
        }
    }
}
