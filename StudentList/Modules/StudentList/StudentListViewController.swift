//
//  StudentListViewController.swift
//  StudentList
//
//  Created by Артур on 04.05.2021.
//

import UIKit

class StudentListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var tableView: UITableView!
    var students = [Student]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Список учеников"
        
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addButtonDidTap))
        navigationItem.rightBarButtonItem = addButton
        
        tableView.register(UINib(nibName: "StudentCell", bundle: nil), forCellReuseIdentifier: "StudentCell")
        tableView.rowHeight = 44
        
        getStudentsFromRealm()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getStudentsFromRealm()
        tableView.reloadData()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return students.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StudentCell") as? StudentCell
        cell?.fillCell(student: students[indexPath.row])
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        openStudentDetailed(student: students[indexPath.row])
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            let realmManager = RealmManager()
            
            // Можно удалить из рилма только тот объект, который из него достали. Иначе краш
            if let realmResults = realmManager.obtain(objectType: RealmStudent.self) {
                let realmStudentsToDelete = Array(realmResults).filter { $0.id == students[indexPath.row].id }
                realmManager.delete(objects: realmStudentsToDelete)
            }
            
            students.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic) // С анимацией
        }
    }
    
    @objc func addButtonDidTap() {
        openStudentDetailed(student: nil)
    }
    
    func openStudentDetailed(student: Student?) {
        let studentDetailedViewController = StudentDetailedViewController(student: student)
        navigationController?.pushViewController(studentDetailedViewController, animated: true)
    }
    
    func getStudentsFromRealm() {
        if let realmResults = RealmManager().obtain(objectType: RealmStudent.self) {
            self.students = realmResults.map { Student(realmStudent: $0) }.sorted { $0.surname < $1.surname }
        }
    }
}
