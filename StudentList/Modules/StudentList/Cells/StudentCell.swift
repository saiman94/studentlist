//
//  StudentCell.swift
//  StudentList
//
//  Created by Артур on 04.05.2021.
//

import UIKit

class StudentCell: UITableViewCell {

    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var gradeLabel: UILabel!
    
    func fillCell(student: Student) {
        nameLabel.text = student.name + " " + student.surname
        gradeLabel.text = String (student.grade)
    }
    
}
