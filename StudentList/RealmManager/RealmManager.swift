//
//  RealmManager.swift
//  StudentList
//
//  Created by Артур on 04.05.2021.
//

import Foundation
import RealmSwift

class RealmManager {
    
    func write<T: Object>(object: [T]) {
        let realm = try! Realm()
        try! realm.write {
            realm.add(object, update: .modified)
        }
    }
    
    func obtain<T: Object>(objectType: T.Type) -> Results<T>? {
        let realm = try! Realm()
        return realm.objects(objectType)
    }
    
    func delete<T: Object>(objects: [T]) {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(objects)
        }
    }
}
