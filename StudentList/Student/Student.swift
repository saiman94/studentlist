//
//  Student.swift
//  StudentList
//
//  Created by Артур on 04.05.2021.
//

import Foundation
import RealmSwift

struct Student {
    var id: String
    
    var name: String
    var surname: String
    var grade: Int
    
    init(name: String, surname: String, grade: Int) {
        self.id = UUID().uuidString // при создании нового студента генерируем уникальный айдишник
        self.name = name
        self.surname = surname
        self.grade = grade
        print("init struct")
    }
    
    init(realmStudent: RealmStudent) {
        self.id = realmStudent.id
        self.name = realmStudent.name
        self.surname = realmStudent.surname
        self.grade = realmStudent.grade
    }
}

class RealmStudent: Object {
    @objc dynamic var id: String = ""
    
    @objc dynamic var name: String = ""
    @objc dynamic var surname: String = ""
    @objc dynamic var grade: Int = 0
    
    convenience init(student: Student) {
        self.init()
        self.id = student.id
        self.name = student.name
        self.surname = student.surname
        self.grade = student.grade
    }
    
    // записывается в рилм по айдишнику, чтобы можно было изменять существующих студентов
    override class func primaryKey() -> String? {
        return "id"
    }
}
